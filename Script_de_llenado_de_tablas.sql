

insert into HANGAR values('15','1');
insert into HANGAR values('20','2');
insert into HANGAR values('25','3');
insert into HANGAR values('30','4');
insert into HANGAR values('35','5');

insert into TIPO_AVION values('150','100000','Airbus','1');
insert into TIPO_AVION values('150','100000','Boeing','2');
insert into TIPO_AVION values('150','100000','Mig','3');
insert into TIPO_AVION values('150','100000','Su','4');
insert into TIPO_AVION values('150','100000','Jaguar','5');

insert into PILOTO values('Basthian M','1');
insert into PILOTO values('Nelson J','2');
insert into PILOTO values('WladimirR','3');
insert into PILOTO values('Maria','4');
insert into PILOTO values('Nicol B','5');
insert into PILOTO values('Camila L','6');

insert into PASAJERO values('Pasajero 1','12345678','1');
insert into PASAJERO values('Pasajero 2','12345678','2');
insert into PASAJERO values('Pasajero 3','12345678','3');
insert into PASAJERO values('Pasajero 4','12345678','4');
insert into PASAJERO values('Pasajero 5','12345678','5');
insert into PASAJERO values('Pasajero 6','12345678','6');
insert into PASAJERO values('Pasajero 7','12345678','7');
insert into PASAJERO values('Pasajero 8','12345678','8');
insert into PASAJERO values('Pasajero 9','12345678','9');

insert into PROPIETARIO values('1','Persona','Juan B','Direccion','1');
insert into PROPIETARIO values('2','Persona','Maria A','Direccion','2');
insert into PROPIETARIO values('3','Persona','Assuan','Direccion','3');
insert into PROPIETARIO values('4','Persona','Banco Chile','Direccion','4');
insert into PROPIETARIO values('5','Persona','Banco Estado','Direccion','5');
insert into PROPIETARIO values('6','Corporacion','Concepción 1','Direccion','1');
insert into PROPIETARIO values('7','Corporacion','Concepción 2','Direccion','2');
insert into PROPIETARIO values('8','Corporacion', 'Concepción 3','Direccion','3');
insert into PROPIETARIO values('9','Corporacion','Concepción 4','Direccion','4');
insert into PROPIETARIO values('10','Corporacion','Concepción 5','Direccion','5');

insert into ARRIENDA values('21-12-2017','06-08-2017','150000','1','1','1');
insert into ARRIENDA values('21-12-2015','06-09-2017','150000','2','2','2');
insert into ARRIENDA values('06-08-2016','06-09-2017','150000','3','3','3');

insert into AVION values('1','1','1','1');
insert into AVION values('2','2','2','2');
insert into AVION values('3','3','3','3');
insert into AVION values('4','4','4','4');

insert into VUELO values('1','22-07-2017','06-08-2017','Concepción','Santiago','1');
insert into VUELO values('2','21-07-2017','06-08-2017','Santiago','Osorno','2');
insert into VUELO values('3','06-07-2017','06-08-2017','Santiago','Concepción','3');
insert into VUELO values('4','06-07-2017','06-08-2017','Valpo','Osorno','4');
insert into VUELO values('5','06-07-2017','06-08-2017','Arica','Osorno','1');
insert into VUELO values('6','06-07-2017','06-08-2017','Arica','Osorno','2');
insert into VUELO values('7','06-07-2017','06-08-2017','Pto Montt','Osorno','3');
insert into VUELO values('8','06-07-2017','06-08-2017','Santiago','Osorno','4');
insert into VUELO values('9','06-07-2017','06-08-2017','Santiago','Osorno','1');
insert into VUELO values('10','06-07-2017','06-08-2017','Santiago','Osorno','2');
insert into VUELO values('11','06-07-2017','06-08-2017','Santiago','Osorno','3');
insert into VUELO values('12','06-07-2017','06-08-2017','Santiago','Osorno','4');
insert into VUELO values('13','06-07-2017','06-08-2017','Santiago','Osorno','1');
insert into VUELO values('14','06-07-2017','06-08-2017','Santiago','Concepción','2');
insert into VUELO values('15','06-07-2017','06-08-2017','Valpo','Osorno','3');
insert into VUELO values('16','06-07-2017','06-08-2017','Arica','Osorno','4');
insert into VUELO values('17','06-07-2017','06-08-2017','Arica','Osorno','1');
insert into VUELO values('18','06-07-2017','06-08-2017','Pto Montt','Osorno','2');
insert into VUELO values('19','06-07-2017','06-08-2017','Santiago','Osorno','3');
insert into VUELO values('20','06-07-2017','06-08-2017','Santiago','Osorno','4');

insert into MANTENIMIENTO values('06-07-2017','06-08-2017','180','Afinacion completa','1','1');
insert into MANTENIMIENTO values('06-07-2017','06-08-2017','180','Afinacion completa','2','2');
insert into MANTENIMIENTO values('06-07-2017','06-08-2017','180','Afinacion completa','3','3');
insert into MANTENIMIENTO values('06-07-2017','06-08-2017','180','Afinacion completa','4','4');

insert into MANEJA values('1','1');
insert into MANEJA values('2','2');
insert into MANEJA values('3','3');
insert into MANEJA values('4','4');
insert into MANEJA values('6','5');
insert into MANEJA values('7','6');
insert into MANEJA values('8','1');
insert into MANEJA values('9','2');
insert into MANEJA values('10','3');
insert into MANEJA values('11','4');

insert into VIAJA values('1','1');
insert into VIAJA values('2','1');
insert into VIAJA values('3','1');
insert into VIAJA values('4','1');
insert into VIAJA values('5','1');
insert into VIAJA values('6','1');
insert into VIAJA values('7','1');
insert into VIAJA values('8','1');
insert into VIAJA values('9','1');
insert into VIAJA values('10','1');
insert into VIAJA values('11','2');
insert into VIAJA values('12','2');
insert into VIAJA values('13','2');
insert into VIAJA values('14','2');
insert into VIAJA values('15','2');
insert into VIAJA values('16','2');
insert into VIAJA values('17','3');
insert into VIAJA values('18','3');
insert into VIAJA values('19','3');
