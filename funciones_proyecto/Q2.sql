"Realizar un procedimiento almacenado que reciba como parámetro de entrada
el código del vuelo y que muestre fecha de salida y llegada, con sus
respectivas horas, ademas de la cantidad de pasajeros."

CREATE OR REPLACE PROCEDURE Mostrar_vuelo(codigo NUMBER)
  
  IS

      fechaSalida DATE;
      fechaLlegada DATE;
      cantidadPasajeros INTEGER :=0;
  
  BEGIN

    SELECT VUELO.FECHA_SALIDA, VUELO.FECHA_LLEGADA, COUNT(V.ID_PASAJERO)
      INTO fechaSalida, fechaLlegada, cantidadPasajeros
    FROM VUELO
      JOIN VIAJA V ON VUELO.ID_VUELO = V.ID_VUELO
    WHERE VUELO.ID_VUELO=codigo
      GROUP BY VUELO.FECHA_SALIDA, VUELO.FECHA_LLEGADA;
    
    DBMS_OUTPUT.PUT_LINE('Fecha de Salida : '|| fechaSalida);
    DBMS_OUTPUT.PUT_LINE('Fecha de Llegada : '|| fechaLlegada);
    DBMS_OUTPUT.PUT_LINE('Cantidad Pasajeros : '|| cantidadPasajeros);

END Mostrar_vuelo;


EXECUTE Mostrar_vuelo(1);