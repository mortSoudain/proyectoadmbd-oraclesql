"Realizar un trigger, que valide que un pasajero no puede viajar en dos vuelos el mismo día,
con el mismo destino. Es decir si Diego necesita viajara Antofagasta, no puede estar registrado
para otro vuelo a Antofagasta al menos en el mismo día."

CREATE OR REPLACE TRIGGER RESERVA_VUELO BEFORE INSERT ON VUELO
	
	FOR EACH ROW
	DECLARE

		FECHA_SAL DATE;
		CURSOR C_FECHA IS 
			SELECT FECHA_SALIDA FROM VUELO
			WHERE FECHA_SALIDA = :NEW.FECHA_SALIDA 
			AND DESTINO = :NEW.DESTINO ;

	BEGIN

		OPEN C_FECHA;
		FETCH C_FECHA INTO FECHA_SAL;

		WHILE C_FECHA%FOUND
			LOOP
				IF (TO_CHAR(FECHA_SAL, 'DD/MM/YYYY') = TO_CHAR(:NEW.FECHA_SALIDA, 'DD/MM/YYYY')) THEN
					raise_application_error (-20300,'TIENE RESERVA ESE DIA');
				END IF;
				FETCH C_FECHA INTO FECHA_SAL;
			END LOOP;
		CLOSE C_FECHA;

	END;

"Script de prueba"
insert into VUELO values('25','15-07-2017','15-08-2017','Concepci�n','Santiago','2');
insert into VUELO values('25','15-07-2017','15-08-2017','Concepci�n','Santiago','3');