"El manejo de usuarios y sus respectivos privilegios. Deben existir dos tipos
de usuarios, un usuario que sólo puede consultar datos, pero no insertar, actualziar
ni eliminar datos. Y un usuario que puede administrar completamente la
base de datos."

CREATE USER proyecto IDENTIFIED BY proyecto DEFAULT TABLESPACE users;
GRANT DBA TO proyecto;

CREATE USER encargado IDENTIFIED BY encargado DEFAULT TABLESPACE users;
GRANT CREATE SESSION TO encargado;

GRANT SELECT ON AVION TO encargado WITH GRANT OPTION;
GRANT SELECT ON VIAJA TO encargado WITH GRANT OPTION;
GRANT SELECT ON MANEJA TO encargado WITH GRANT OPTION;
GRANT SELECT ON VUELO TO encargado WITH GRANT OPTION;
GRANT SELECT ON ARRIENDA TO encargado WITH GRANT OPTION;
GRANT SELECT ON PROPIETARIO TO encargado WITH GRANT OPTION;
GRANT SELECT ON CORPORACION TO encargado WITH GRANT OPTION;
GRANT SELECT ON PASAJERO TO encargado WITH GRANT OPTION;
GRANT SELECT ON TIPO_DE_AVION TO encargado WITH GRANT OPTION;
GRANT SELECT ON HANGAR TO encargado WITH GRANT OPTION;
GRANT SELECT ON PILOTO TO encargado WITH GRANT OPTION;
GRANT SELECT ON MANTENIMIENTO TO encargado WITH GRANT OPTION;

//otra opcion
GRANT SELECT ON AVION TO PUBLIC;

DROP USER proyecto CASCADE;
DROP USER encargado CASCADE;

CONNECT proyecto/proyecto;
CONNECT encargado/encargado;