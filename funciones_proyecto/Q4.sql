﻿"Realizar un procedimiento almacenado que permita clasificar sus clientes,
de acuerdo a la cantidad de viajes realizados en el último a~no. Los pasajero
frecuente son quienes han viajado 10 veces, los pasajeros comunes 9 y 5,
los pasajers que han viajado menos de 5 veces son ocasionales."


DECLARE

CURSOR cclientes IS
	SELECT COUNT (V.ID_PASAJERO)
	FROM VIAJA V
    WHERE V.ID_PASAJERO=3;

vuelos NUMBER := 0;

BEGIN

    OPEN cclientes;

        FETCH cclientes INTO vuelos;
        WHILE cclientes%found LOOP

            FETCH cclientes INTO vuelos;
            IF vuelos>=10 THEN
                DBMS_OUTPUT.PUT_LINE('Viajero Frecuente ');
            ELSIF vuelos>=5 THEN
                DBMS_OUTPUT.PUT_LINE('Viajero Comun');
            ELSE
                DBMS_OUTPUT.PUT_LINE('Viajero Ocacional ');
            END IF;
        END LOOP;
    CLOSE cclientes;

END;
