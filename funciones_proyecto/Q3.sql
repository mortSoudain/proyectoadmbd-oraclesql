
"Realizar un procedimiento almacenado que entregue el listado (nombre,
direccion y tipo de todos los propietarios morosos y el monto que deben
cancelar."

CREATE OR REPLACE PROCEDURE Listado_morosos
  
  IS

    CURSOR CUR_MOROSOS IS
        SELECT P.NOMBRE, P.TIPO_PROPIETARIO, P.DIRECCION, A.MONTO
        FROM PROPIETARIO P
          JOIN ARRIENDA A ON P.ID_PROPIETARIO = A.ID_PROPIETARIO
        WHERE A.FECHA_VENCIMIENTO<sysdate;

    PROPIETARIO_MOROSO CUR_MOROSOS%ROWTYPE;

  BEGIN

  OPEN CUR_MOROSOS;
    FETCH CUR_MOROSOS INTO PROPIETARIO_MOROSO;

    WHILE CUR_MOROSOS%FOUND
      LOOP

          DBMS_OUTPUT.PUT_LINE('Propietario : '|| PROPIETARIO_MOROSO.NOMBRE);
          DBMS_OUTPUT.PUT_LINE('Tipo : '|| PROPIETARIO_MOROSO.TIPO_PROPIETARIO);
          DBMS_OUTPUT.PUT_LINE('Direccion : '|| PROPIETARIO_MOROSO.direccion);
          DBMS_OUTPUT.PUT_LINE('Monto Deuda: '|| PROPIETARIO_MOROSO.monto);

        FETCH CUR_MOROSOS INTO PROPIETARIO_MOROSO;
      END LOOP;
  CLOSE CUR_MOROSOS;


END Listado_morosos;


EXECUTE Listado_morosos;
